import sqlite3
from flask import Flask, session, g, jsonify
from web_error import WebError
from helper_methods import end_game, row_to_dict
import logging
import random

logger = logging.getLogger()

app = Flask(__name__)
app.config.from_mapping(
    DATABASE="face_game.sqlite",
)
# using a weak secret key for testing/signing cookies
# this value should be read from a config file on real environments
# the session token can be decrypted, because it is signed by my public key
app.secret_key = b'testSecretKey'


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(app.config['DATABASE'])
        db.row_factory = sqlite3.Row
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def grab_random_user_count(count):
    c = get_db().cursor()
    try:
        # performance hit for large data sets due to full table scan
        query = "SELECT * FROM employees ORDER BY RANDOM() LIMIT ?;"
        profiles = list(c.execute(query, [count]).fetchall())
        validate_query_count(profiles, count, query)
        return profiles
    finally:
        if c is not None:
            c.close()


def validate_query_count(profiles, count, query):
    profile_count = len(profiles)
    if profile_count != count:
        message = "Only found %d/%d records when executing query '%s'.".format(profile_count, count, query)
        logger.error(message)
        raise WebError(message, status_code=500)


@app.route("/startgame", methods=['GET'])
def start_game_route():
    if "active_game" in session:
        raise WebError("You already have an active game.")

    profiles = grab_random_user_count(6)
    correct_guess = row_to_dict(profiles[random.randint(0, 5)])
    session["active_game"] = correct_guess
    image_ids = [{"id": p["id"], "headshotUrl": p["headshotUrl"]} for p in profiles]
    ret_payload = {
        "firstName": correct_guess["firstName"],
        "lastName": correct_guess["lastName"],
        "imageIds": image_ids
    }
    return jsonify(ret_payload)


@app.route("/endgame", methods=['GET'])
def end_game_route():
    return jsonify(end_game(session))


@app.route("/guess/<id>", methods=['GET'])
def guess_route(id):
    active_game = session.get("active_game", None)
    if active_game is None:
        raise WebError("You have no active games, you must start a game before guessing!")

    logging.debug("Guessed id:'%s'.", id)
    is_correct_guess = active_game["id"] == id
    ret_val = {"correctGuess": is_correct_guess}
    if is_correct_guess:
        ret_val = {**ret_val, **end_game(session)}
    return jsonify(ret_val)


@app.errorhandler(WebError)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    logging.info("Returned error message '{}'.", response)
    return response
