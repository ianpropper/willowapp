## How to setup (Linux instructions)

1 . Ensure you have python3 or higher installed

2 . Install virtualenv if you do not already have it
```
python3 -m pip install --user virtualenv
```
3 . Create a virtual environment
```
python3 -m virtualenv env
source env/bin/activate
```
4 .  Install requirements and setup the database

```
pip install -r requirements.txt
python setup_database.py
pytest
```
5 . Now a database should be setup and all test should be passing!

## Play A Game
 Run flask with the following command
```
export FLASK_APP=server.py
flask run
```
Now we can play a game! 

Flask should start on `localhost:5000`. To start, enter a browser and navigate to:
```
localhost:5000/startgame
```

You will receive a list of image + id pairs, a first name, and a last name.
To make a guess enter in one of the user ids at 
`http://localhost:5000/guess/<id>`
for  example:

```
http://localhost:5000/guess/1234
```

When you guess correctly the game will end!

You can also end the game early by going to:
```
http://localhost:5000/endgame
```

## Design Decisions

### Sqllite DB 

Sqlite is great for working with small sets of data because it is 
an embedded database. It can easily be setup and tore down, making it very demo-able.
I choose a relational DB because it is easy to randomly sample data, and 
implementing future game logic, such as querying all Matts, would fit well with a WHERE statement. 

As the data set grew in size, sqlite would be a bad choice, and we would want to shift to a datastore 
or data model that was optimized for random sampling with large sets of data. Random sampling is a core feature 
because we want each face game to present 6 random unique records, and we want those records to be 
different each game. 

### Python + Flask

I chose to use python and the flask framework because I wanted to learn more about flask,
and I felt flask had a very minimal design. Something like java is great for very large projects
because java has a type system, is multi-threaded out of the box, and can be better optimized. 
However, for this small project those features were not needed. 
I thought the most important features were readability and time to write the code, 
and I think python and flask offer that. 

### Additional Development 

These are the features I wanted to add but did not have time:

-  Make the site https with valid certs.

-  Store the correct game result in the DB instead of the session cookie. 
   Right now a user can cheat and get the right answer by decrypting the cookie. 
   Cookies are cryptographically signed, but not encrypted.
 
-  Login, users and passwords. This enables user based statistics and features such as a scoreboard and persisting games between sessions. 

