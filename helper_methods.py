from web_error import WebError


def end_game(game_session):
    if "active_game" in game_session:
        del game_session["active_game"]
        return {"message": "Ended the game."}
    else:
        raise WebError("Cannot end the game because you have no active games.")


def row_to_dict(row):
    return dict(zip(row.keys(), row))
