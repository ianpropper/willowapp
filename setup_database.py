import requests
import logging
import sqlite3

logger = logging.getLogger()
image_set = set()
dup_images = set()


# This script is used to pull from the willowtree api and initialize my database
def initialize_db():
    r = requests.get("https://www.willowtreeapps.com/api/v1.0/profiles")
    profiles = r.json()
    profiles = [p for p in profiles if has_required_dems(p)]
    for prof in profiles:
        aggregate_dup_images(prof)

    valid_profiles = filter(lambda p: p["headshot"]["url"] not in dup_images, profiles)
    create_and_populate_db(valid_profiles)


def has_required_dems(person):
    missing_keys = {"id", "firstName", "lastName", "headshot"} - person.keys()
    if "headshot" in person and "url" not in person["headshot"]:
        missing_keys.add("headshot.url")
    if len(missing_keys) > 0:
        logger.warning("The following required demographics '%s' are missing from person '%s'.", missing_keys, person)
        return False
    return True


def aggregate_dup_images(person):
    image_url = person["headshot"]["url"]
    if image_url in image_set:
        logger.warning("Duplicate image detected '%s'", image_url)
        dup_images.add(image_url)
    image_set.add(image_url)


def create_and_populate_db(people):
    conn = sqlite3.connect('face_game.sqlite')
    with conn as con:
        c = con.cursor()

        c.execute("CREATE TABLE IF NOT EXISTS employees "
                  "(id varchar(100) primary key, "
                  "firstName varchar(100), "
                  "lastName varchar(100), "
                  "headshotUrl TEXT);")

        for p in people:
            c.execute("INSERT OR REPLACE INTO employees VALUES (?,?,?,?);",
                      [p["id"], p["firstName"], p["lastName"], p["headshot"]["url"]])
        con.commit()


if __name__ == '__main__':
    initialize_db()
