import pytest
import json
from server import app
from setup_database import initialize_db


def setup_module(module):
    initialize_db()


@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['DATABASE'] = "face_game.sqlite"
    client = app.test_client()
    yield client


def test_404(client):
    rv = client.get('/random')
    assert rv.status_code == 404


def test_end_inactive_game(client):
    rv = client.get('/endgame')
    assert rv.status_code == 400
    assert "Cannot end the game because you have no active games." == json.loads(rv.data)["message"]


def test_start_end(client):
    client.get("/startgame")
    rv = client.get('/endgame')
    assert rv.status_code == 200
    assert json.loads(rv.data) == {"message": "Ended the game."}


def test_false_start(client):
    rv = client.get("/startgame")
    assert rv.status_code == 200
    rv = client.get("/startgame")
    assert rv.status_code == 400
    assert json.loads(rv.data) == {"message": "You already have an active game."}


def test_empty_guess(client):
    rv = client.get('/guess/abc')
    assert rv.status_code == 400
    assert "You have no active games, you must start a game before guessing!" == json.loads(rv.data)["message"]


def test_start_guess_end(client):
    pos = set()
    for i in range(0, 15):
        rv = client.get("/startgame")
        options = json.loads(rv.data)['imageIds']
        answer = {}
        for i in range(0, len(options)):
            o = options[i]
            rv = client.get("/guess/{}".format(o["id"]))
            response = json.loads(rv.data)
            if response["correctGuess"]:
                answer = response
                pos.add(i)
                break
        assert answer == {"correctGuess": True, "message": "Ended the game."}
    # is answer position randomly distributed?
    assert len(pos) > 1
